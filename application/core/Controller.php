<?php

/**
 * Базовый контроллер
 */
abstract class Controller
{
    /**
     * @var Model - для хранения объекта Model
     */
    public $model;

    /**
     * @var View - для хранения объекта View
     */
    public $view;

    /**
     * Создает экземпляр класса View и сохраняет его в свойство View
     */
    public function __construct()
    {
        $this->view = new View();
    }
}