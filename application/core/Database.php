<?php

/**
 * Singleton
 *
 * Mysql database class
 */
class Database
{
    public $_connection;
    private static $_instance;


    /**
     * Get an instance of the Database
     *
     * @return Database class instance
     */
    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Database constructor.
     */
    private function __construct()
    {
        $this->_connection = new PDO('mysql:dbname=su;host=localhost', 'root', '');
    }

    /**
     * Magic method clone is empty to prevent duplication of connection
     */
    private function __clone()
    {
    }

    /**
     * SELECT constructor
     *
     * @param array $params Массив с данными для запроса
     *
     * (string) $params['fields'] - поля которые хотим получить
     * (string)$params['from'] - имя таблицы
     * (string|array) $params['join'] - условия объединения таблиц
     * (string) $params['where'] - условия
     *
     * @return array $result
     */
    public function find($params = array())
    {
        $query = "SELECT {$params['fields']} ";
        $query .= "FROM {$params['from']} ";

        // join
        if (isset($params['join'])) {
            if (is_array($params['join'])) {
                foreach ($params['join'] as $join) {
                    $query .= $join . ' ';
                }
            } elseif (is_string($params['join'])) {
                $query .= "FROM {$params['join']} ";
            }
        }

        // where
        if (isset($params['where'])) {
            $query .= "WHERE {$params['where']}";
        }

        $stmt = $this->_connection->prepare(trim($query));
        //$stmt->bindParam(':id', $id, PDO::PARAM_INT); // если усложнить  
        $stmt->execute();
        $records = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $records;
    }


    /**
     * INSERT constructor
     *
     * @param string $tableName
     * @param array $arg - Parameters for insert
     * Example: $arg = ['status_id' => ['value' => 1,'type' => PDO::PARAM_INT,]]
     *
     * @return int $lastId - Last inserted id
     * @throws Exception
     */
    public function insert($tableName, $arg)
    {
        $this->_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        try {
            // prepare query
            $query = "INSERT INTO {$tableName} SET";
            foreach ($arg as $field => $param) {
                $query .= " {$field} = :{$field},";
            }
            $query = rtrim($query, ',');
            $stmt = $this->_connection->prepare($query);

            // bind parameters
            foreach ($arg as $field => $param) {
                $stmt->bindParam(":{$field}", $param['value'], $param['type']);
            }
            $stmt->execute();

            $lastId = $this->_connection->lastInsertId();
        } catch (PDOException $e) {
            throw new Exception($e->getMessage() . "  " . get_class($this) . ' -> ' . __METHOD__);
        }
        return (int)$lastId;
    }

    /**
     * UPDATE constructor
     *
     * @param string $tableName
     * @param array $id
     * Example: $id = ['name' => "order_id", "value" => $orderId]
     * @param array $arg - Parameters for Update
     * Example: $arg = ['status_id' => ['value' => 1,'type' => PDO::PARAM_INT,]]
     *
     * @return bool - Update result
     * @throws Exception
     */
    public function update($tableName, $id, $arg)
    {
        $this->_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        try {
            // prepare query
            $query = "UPDATE {$tableName} SET";

            foreach ($arg as $field => $param) {
                $query .= " {$field} = :{$field},";
            }
            $query = rtrim($query, ',');

            $query .= " WHERE {$id['name']} = :{$id['name']}";

            $stmt = $this->_connection->prepare($query);

            // bind parameters
            $stmt->bindParam(":{$id['name']}", $id['value'], PDO::PARAM_INT);
            foreach ($arg as $field => $param) {
                $stmt->bindParam(":{$field}", $param['value'], $param['type']);
            }
            $result = $stmt->execute();

        } catch (PDOException $e) {
            throw new Exception($e->getMessage() . "  " . get_class($this) . ' -> ' . __METHOD__);
        }
        return $result;
    }
}
