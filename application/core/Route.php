<?php

class Route
{
    public static function start()
    {
        // контроллер и действие по умолчанию
        $controller_name = 'main';
        $action_name = 'index';
        $params = null;

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        // получаем имя контроллера
        if (!empty($routes[2])) {
            $controller_name = $routes[2];
        }

        // получаем имя экшона
        if (!empty($routes[3])) {
            $action_name = $routes[3];
        }

        // получаем параметры
        if (!empty(array_slice($routes, 4))) {
            $params = $routes[4];
        }

        // добавляем префиксы
        $model_name = 'Model_' . $controller_name;
        $controller_name = 'Controller_' . $controller_name;
        $action_name = 'action_' . $action_name;

        // подцепляем файл с классом модели (файла модели может и не быть)
        $model_file = strtolower($model_name) . '.php';
        $model_path = 'application/models/' . $model_file;
        if (file_exists($model_path)) {
            include 'application/models/' . $model_file;
        } else {
            die("Model file '{$model_file}'' does not exists!");
        }

        // подцепляем файл с классом контроллера
        $controller_file = strtolower($controller_name) . '.php';
        $controller_path = 'application/controllers/' . $controller_file;
        if (file_exists($controller_path)) {
            include 'application/controllers/' . $controller_file;
        } else {
            // Правильно было бы кинуть тут исключение
            die("Controller file '{$controller_file}'' does not exists!");
        }

        // Check if the class has been defined
        if (!class_exists($controller_name)) {
            die("Controller '{$controller_name}' does not exists!");
        } else {
            $controller = new $controller_name;
            $action = $action_name;
            
            // Check if the class method exists
            if (method_exists($controller, $action)) {
                // вызываем действие контроллера

                if ($params) {
                    $controller->$action($params);
                } else {
                    $controller->$action();
                }

            } else {
                // здесь также правильнее кинуть исключение
                die("Method '{$action}' does not exists!");
            }
        }
    }

    public static function ErrorPage404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP 1.1 404 not found');
        header('Status: 404 not found');
        header('Location:' . $host . '404');
    }
}