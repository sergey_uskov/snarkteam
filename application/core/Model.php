<?php

/**
 * Базовая модель
 */
abstract class Model
{
    /**
     * @var object PDO - хранит объект PDO
     */
    protected $_pdo;

    /**
     * Создает объект PDO для работы с БД и сохраняет в свойство $_pdo
     */
    public function __construct()
    {
        $this->_pdo = Database::getInstance();
    }
}