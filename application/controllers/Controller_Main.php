<?php

/**
 * Контроллер главной страницы (по умолчанию)
 */
class Controller_Main extends Controller
{
    public function __construct()
    {
        parent::__construct();
        
        // По идее контроллер Main и модель Main прописаны в роутере по умолчанию 
        $this->model = new Model_Main();
    }

    /**
     * Главная страница
     */
    public function action_index()
    {
        $this->view->generate('content_view.php', 'template_view.php',
            array(
                'title' => 'Главная страница',
                'slider' => true,
                'header' => true,
                'menu' => true,
                'footer' => true,
            )
        );
    }
}