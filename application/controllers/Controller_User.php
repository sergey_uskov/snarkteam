<?php


class Controller_User extends Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->model = new Model_User();
    }

    /**
     * Страница регистрации пользователя
     */
    public function action_register()
    {
        $this->view->generate('content_view.php', 'template_view.php',
            array(
                'title' => 'Регистрация',
            )
        );
    }

    /**
     * Личный кабинет пользователя
     */
    public function action_personal($user_id)
    {
        $this->view->generate('content_view.php', 'template_view.php',
            array(
                'title' => 'Личный кабинет',
                'user_info' => $this->model->get_by_id(),
                'user_orders' => $this->model->get_user_orders($user_id),
            )
        );
    }
}