<?php

/**
 * 
 */
class Controller_Products extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->model = new Model_Products();
    }
    
    public function action_index()
    {
        $this->view->generate('content_view.php', 'template_view.php',
            array(
                'title' => 'Список товаров',
                'products' => $this->model->get_data(),
            )
        );
    }
    
    public function action_view()
    {
        $this->view->generate('content_view.php', 'template_view.php',
            array(
                'title' => 'Просмотр товара',
                //'items_by_category' => $this->model->get_by_category($product_id),
            )
        );
    }
}