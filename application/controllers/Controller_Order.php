<?php

/**
 *
 */
class Controller_Order extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->model = new Model_Order();
    }

    /**
     * Отображает страницу с корзиной покупателя
     */
    public function action_cart()
    {
        $this->view->generate('content_view.php', 'template_view.php',
            array(
                'title' => 'Корзина',
                //'products_list' => $this->model->get_cart(),
            )
        );
    }

    /**
     * Страница оформления заказа
     */
    public function action_checkout()
    {
        $this->model->new_order();
//        $this->view->generate('content_view.php', 'template_view.php',
//            array(
//                'title' => 'Оформление заказа',
//                'new_order' => $this->model->save_order(),
//            )
//        );
    }


}