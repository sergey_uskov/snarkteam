<?php

/**
 *
 */
class Model_User extends Model
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * Авторизует пользователя
     */
    public function login()
    {
        // checks
        
        // set $this->get_info() result to session with logged flag
    }

    /**
     * Разлогинивает пользователя
     */
    public function logout()
    {
        // destroy session
    }

    /**
     * Регистрирует нового пользователя
     */
    public function register()
    {
        // save new user
        // send email with login parameters and link to personal area
        // login
    }

    /**
     * Сохраняет данные о пользователе
     */
    public function save_user()
    {

    }

    /**
     * Возвращает информацию о пользователе
     */
    public function get_by_id()
    {

    }

    /**
     * Возвращает информацию о пользователе
     */
    public function get_user_orders()
    {

    }
}