<?php

/**
 *
 */
class Model_Order extends Model
{
    /**
     * Возвращает содержимое корзины пользователя
     */
    public function get_cart()
    {

    }

    /**
     * Сохраняет заказ
     * Регистрирует и логинит юзера, если он новенький
     * Редирект в личный кабинет
     */
    public function new_order()
    {
        // order save
        $orderParams = [
            'status_id' => [
                'value' => 1,
                'type' => PDO::PARAM_INT,
            ],
            'order_cost' => [
                'value' => "200.00",
                'type' => PDO::PARAM_STR,
            ],
            'customer_id' => [
                'value' => 5,
                'type' => PDO::PARAM_INT,
            ],
            'employee_id' => [
                'value' => 10,
                'type' => PDO::PARAM_INT,
            ],
            'payment_id' => [
                'value' => 2,
                'type' => PDO::PARAM_INT,
            ],
            'delivery_id' => [
                'value' => 0,
                'type' => PDO::PARAM_INT,
            ],
            'created_at' => [
                'value' => date('Y-m-d H:i:s'),
                'type' => PDO::PARAM_STR,
            ],
            'updated_at' => [
                'value' => date('Y-m-d H:i:s'),
                'type' => PDO::PARAM_STR,
            ],
        ];

        $orderId = $this->_pdo->insert("orders", $orderParams);

        // order_products save
        $orderProductsParams = [
            'order_id' => [
                'value' => $orderId,
                'type' => PDO::PARAM_INT,
            ],
            'product_id' => [
                'value' => 4,
                'type' => PDO::PARAM_INT,
            ],
            'qtty' => [
                'value' => 5,
                'type' => PDO::PARAM_INT,
            ],
            'price' => [
                'value' => "40.00",
                'type' => PDO::PARAM_STR,
            ],
            'price_modifier' => [
                'value' => 1.2,
                'type' => PDO::PARAM_STR,
            ],
        ];
        //$this->_pdo->insert("order_products", $orderProductsParams);

        // delivery save
        $deliveryParams = [
            'order_id' => [
                'value' => $orderId,
                'type' => PDO::PARAM_INT,
            ],
            'employee_id' => [
                'value' => 4,
                'type' => PDO::PARAM_INT,
            ],
            'total_weight' => [
                'value' => 54.23,
                'type' => PDO::PARAM_STR,
            ],
            'total_space' => [
                'value' => 4.50,
                'type' => PDO::PARAM_STR,
            ],
            'distance' => [
                'value' => 45,
                'type' => PDO::PARAM_STR,
            ],
            'delivery_date' => [
                'value' => "2016-06-10",
                'type' => PDO::PARAM_STR,
            ],
            'delivery_cost' => [
                'value' => 45.00,
                'type' => PDO::PARAM_STR,
            ],
            'type_id' => [
                'value' => 1,
                'type' => PDO::PARAM_INT,
            ],
            'status_id' => [
                'value' => 2,
                'type' => PDO::PARAM_INT,
            ],
            'created_at' => [
                'value' => date('Y-m-d H:i:s'),
                'type' => PDO::PARAM_STR,
            ],
            'updated_at' => [
                'value' => date('Y-m-d H:i:s'),
                'type' => PDO::PARAM_STR,
            ],
        ];
        //$this->_pdo->insert("deliveries", $deliveryParams);

        // update order after delivery save
        $orderUpdateParams = [
            'delivery_id' => [
                'value' => 55,
                'type' => PDO::PARAM_INT,
            ],
        ];
        $this->_pdo->update("orders", array('name' => "order_id", "value" => $orderId), $orderUpdateParams);

        echo $orderId;
    }
}