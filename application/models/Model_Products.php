<?php

/**
 * 
 */
class Model_Products extends Model
{

    /**
     * Возвращает список наиболее популярных товаров
     */
    public function get_by_category($category_id)
    {

    }

    /**
     * Возвращает инфо о товаре
     */
    public function get_by_id($product_id)
    {

    }

    /**
     * Созраняет инфо о товаре
     */
    public function save($product_id)
    {

    }


    /**
     *
     *
     * @return array
     */
    public function get_data()
    {
        $arg = array(
            "fields" => "p.product_id, p.title, pa.`name`, pv.attribute_value",
            "from" => "products AS p",
            "join" => array(
                '0' => "LEFT JOIN pr_attributes AS pa ON p.product_id = pa.product_id",
                '1' => "LEFT JOIN pr_attributes_values AS pv ON p.product_id = pv.product_id",
            )
        );
        $result = $this->_pdo->find($arg);

        echo "<pre>";
        var_dump($result);
        echo "</pre>";

        return $result;
    }

    public function get_product($id)
    {
        $sql = "
            SELECT
              products.id,
              products.title,
              products.description,
              category_products.title AS category_name
            FROM products
              LEFT JOIN category_products
                ON products.id_catalog = category_products.id
            WHERE products.id = :id";

        $stmt = $this->_pdo->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $records = $stmt->fetch(PDO::FETCH_ASSOC);

        return $records;
    }
}