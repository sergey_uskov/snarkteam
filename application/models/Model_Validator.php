<?php

/**
 * Class Validator
 */
class Model_Validator
{
    /**
     * @var bool
     */
    public $noErrors = true;

    /**
     * @var string
     */
    public $errorMsg;

    /**
     * Applies specified rule for validation
     * @param mixed $value
     * @param string $rule Valid values: email, integer
     */
    public function check($value, $rule)
    {
        if (!empty($rule)) {
            $this->$rule($value);
        } else {
            die("Error: Invalid validation rule");
        }
    }

    /**
     * Rule for "email" validation
     * @param $value
     */
    private function email($value){
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $this->noErrors = false;
            $this->errorMsg = "Please, specify correct email address!";
        }
    }

    /**
     * Rule for "integer" validation
     * @param $value
     */
    private function integer($value){
        if (!filter_var($value, FILTER_VALIDATE_INT)) {
            $this->noErrors = false;
            $this->errorMsg = "Please, specify correct ID!";
        }
    }
}